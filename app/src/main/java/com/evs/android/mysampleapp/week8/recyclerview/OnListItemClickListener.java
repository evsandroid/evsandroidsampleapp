package com.evs.android.mysampleapp.week8.recyclerview;

public interface OnListItemClickListener {
    void onItemClick(int position, Item item);
}